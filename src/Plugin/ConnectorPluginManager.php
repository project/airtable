<?php

declare(strict_types = 1);

namespace Drupal\airtable\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\airtable\Annotation\AirtableConnector;

/**
 * Provides the Connector plugin manager.
 */
class ConnectorPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new ConnectorPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Airtable/Connector', $namespaces, $module_handler, ConnectorInterface::class, AirtableConnector::class);

    $this->alterInfo('airtable_connector_info');
    $this->setCacheBackend($cache_backend, 'airtable_connector_plugins');
  }

}
