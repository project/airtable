<?php

declare(strict_types = 1);

namespace Drupal\airtable\Plugin;

use DavidZadrazil\AirtableApi\Airtable;
use DavidZadrazil\AirtableApi\Record;
use DavidZadrazil\AirtableApi\Request;
use DavidZadrazil\AirtableApi\Response;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Connector plugins.
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
 */
abstract class ConnectorBase extends PluginBase implements ConnectorInterface, ContainerFactoryPluginInterface {

  /**
   * The airtable connection.
   *
   * @var \DavidZadrazil\AirtableApi\Airtable
   */
  protected $airtable;

  /**
   * The airtable request.
   *
   * @var \DavidZadrazil\AirtableApi\Request
   */
  protected $request;

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * ConnectorBase constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The key repository.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, KeyRepositoryInterface $key_repository, CacheBackendInterface $cache) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cache = $cache;

    $keyId = $config_factory->get('airtable.config')
      ->get('key_id');
    $key = $key_repository->getKey($keyId);

    $this->airtable = new Airtable($key->getKeyValue(), $this->pluginDefinition['base_id']);
    $this->request = new Request($this->airtable, $this->pluginDefinition['name']);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('key.repository'),
      $container->get('cache.airtable')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createRecord(array $record): Response {
    return $this->request->createRecord($record);
  }

  /**
   * {@inheritdoc}
   */
  public function updateRecord($id, array $record): Response {
    return $this->request->updateRecord($id, $record);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRecord($id): Response {
    return $this->request->deleteRecord($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getRecord($id, $reset = FALSE): ?Record {
    if (!$reset && ($cache = $this->cache->get($id))) {
      return $cache->data;
    }

    $response = $this->request->getRecord($id);
    $record = NULL;
    if ($response->isSuccess()) {
      $records = $response->getRecords();
      $record = reset($records);
      $this->cache->set($id, $record, strtotime('+1 hour'));
    }

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function getTable(array $parameters = []): Response {
    return $this->request->getTable($parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function summary(): Response {
    $request = new Request($this->airtable, 'summary');

    return $request->getTable([
      'fields' => [
        sprintf('count--%s%s', $this->pluginDefinition['name'], $this->configuration['summary_suffix'] ?? ''),
      ],
      'filterByFormula' => sprintf('{type} = "%s%s"', $this->pluginDefinition['name'], $this->configuration['summary_type_suffix'] ?? ''),
      'maxRecords' => 1,
    ]);
  }

}
