CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Maintainers

INTRODUCTION
------------

This modules provides a connection between Airtable and Drupal. By exposing
external tables via config entities, the content of those tables can be
used internally.

REQUIREMENTS
------------

- [Key](https://drupal.org/project/key): we use the Key-module in order to store the 
Airtable API Key.


INSTALLATION
------------

Install this module via Composer using `composer req drupal/airtable`.

- Go to `/admin/config/system/keys`;
- Create a new Key with type = 'Authentication';
- Decide where you want to keep the value and click on 'Save';
- Go to `/admin/config/services/airtable/config`;
- Select the newly-created Key and save the configuration;

CONFIGURATION
-------------

Exposing a new Airtable table:

- Go to `/admin/config/services/airtable` and click the button "Add Airtable Table";
- The "Label"-field is free to chose, its purpose is purely descriptive;
- The "Name"-field holds the name of the Airtable table;
- And finally, input the base ID (can also be found on Airtable);

USAGE
-----

The module provides a deriver which will expose your configured Airtable tables via
the AirtableConnector-plugin manager. Eg.:
```php
$manager = \Drupal::service('plugin.manager.airtable.connector');
$connector = $manager->createInstance('airtable_tables:table:[MACHINE_NAME]');
``` 

Next, take a look at [davidzadrazil/airtable-api-php](https://github.com/davidzadrazil/airtable-api-php).
This module uses a fork of that package, but the usage stays the same. This means that the following
methods are available:

```php
$connector->createRecord(['Name' => 'This appears in Name field']);
$connector->updateRecord('recsH5WYbYpwWMlvb', ['Name' => 'Updated value']);
$connector->deleteRecord('recsH5WYbYpwWMlvb');
$connector->getRecord('recsH5WYbYpwWMlvb');
$connector->getTable(['pageSize' => 50, 'filterByFormula' => '{Name} = "test"']);
```

MAINTAINERS
-----------

  * LammensJ - https://www.drupal.org/u/lammensj
