<?php

declare(strict_types = 1);

namespace Drupal\airtable\Plugin\Menu\LocalAction;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TableAddLocalAction.
 */
class TableAddLocalAction extends LocalActionDefault {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * TableAddLocalAction constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The definition.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteProviderInterface $route_provider, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);

    $this->config = $config_factory->get('airtable.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('router.route_provider'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    $key = $this->config->get('key_id');
    if (!$key) {
      return 'airtable.config_form';
    }

    return parent::getRouteName();
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(Request $request = NULL) {
    $key = $this->config->get('key_id');
    if (!$key) {
      $title = new TranslatableMarkup('No API key configured. Add one by clicking here');

      return $title;
    }

    return parent::getTitle($request);
  }

}
