<?php

declare(strict_types = 1);

namespace Drupal\airtable\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Table entities.
 */
interface TableInterface extends ConfigEntityInterface {

  /**
   * Get the name of the table.
   *
   * @return string
   *   Returns the name of the table.
   */
  public function getName();

  /**
   * Set the name of the table.
   *
   * @param string $name
   *   The name of the table.
   *
   * @return \Drupal\airtable\Entity\TableInterface
   *   Returns the called object
   */
  public function setName($name): TableInterface;

  /**
   * Get the base ID of the table.
   *
   * @return string
   *   Returns the base ID of the table.
   */
  public function getBaseId();

  /**
   * Set the base ID of the table.
   *
   * @param string $baseId
   *   The base ID of the table.
   *
   * @return \Drupal\airtable\Entity\TableInterface
   *   Returns the called object
   */
  public function setBaseId($baseId): TableInterface;

}
