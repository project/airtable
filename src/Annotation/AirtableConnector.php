<?php

declare(strict_types = 1);

namespace Drupal\airtable\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Airtable Connector annotation object.
 *
 * @see \Drupal\airtable\Plugin\ConnectorPluginManager
 * @see plugin_api
 *
 * @Annotation
 *
 * @codingStandardsIgnoreFile
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class AirtableConnector extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The name of the Airtable base.
   *
   * @var string
   */
  public $name;

  /**
   * The ID of the Airtable base.
   *
   * @var string
   */
  public $base_id;

}
