<?php

declare(strict_types = 1);

namespace Drupal\airtable\Plugin\Airtable\Connector;

use Drupal\airtable\Plugin\ConnectorBase;

/**
 * Class Tables.
 *
 * @AirtableConnector(
 *   id = "airtable_tables",
 *   label = @Translation("Tables"),
 *   deriver = "\Drupal\airtable\Plugin\Derivative\Tables"
 * )
 */
class Tables extends ConnectorBase {}
