<?php

declare(strict_types = 1);

namespace Drupal\airtable\Plugin;

use DavidZadrazil\AirtableApi\Record;
use DavidZadrazil\AirtableApi\Response;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Connector plugins.
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
interface ConnectorInterface extends PluginInspectionInterface {

  /**
   * Create a new record.
   *
   * @param array $record
   *   The new record to create.
   *
   * @return \DavidZadrazil\AirtableApi\Response
   *   Returns an Airtable response.
   */
  public function createRecord(array $record): Response;

  /**
   * Update an existing record.
   *
   * @param string $id
   *   The ID of the record.
   * @param array $record
   *   The fields and values of the record to update.
   *
   * @return \DavidZadrazil\AirtableApi\Response
   *   Returns an Airtable response.
   */
  public function updateRecord($id, array $record): Response;

  /**
   * Delete an existing record.
   *
   * @param string $id
   *   The id of the record to delete.
   *
   * @return \DavidZadrazil\AirtableApi\Response
   *   Returns an Airtable response.
   */
  public function deleteRecord($id): Response;

  /**
   * Fetch a single record from a table.
   *
   * @param string $id
   *   The id of the record to fetch.
   *
   * @return \DavidZadrazil\AirtableApi\Record|null
   *   Returns an Airtable Record.
   */
  public function getRecord($id): ?Record;

  /**
   * Fetch records from a table.
   *
   * @var array $parameters
   *   The parameters for the request.
   *
   * @return \DavidZadrazil\AirtableApi\Response
   *   Returns an Airtable response.
   */
  public function getTable(array $parameters = []): Response;

  /**
   * Fetch a summary record for the current table.
   *
   * @return \DavidZadrazil\AirtableApi\Response
   *   Returns an Airtable response.
   */
  public function summary(): Response;

}
