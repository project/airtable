<?php

declare(strict_types = 1);

namespace Drupal\airtable\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Tables.
 *
 * @see \Drupal\airtable\Plugin\Airtable\Connector\Tables
 */
class Tables extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Tables constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_def): array {
    $derivatives = parent::getDerivativeDefinitions($base_plugin_def);

    /** @var \Drupal\airtable\Entity\TableInterface[] $tables */
    $tables = $this->entityTypeManager->getStorage('airtable_table')
      ->loadMultiple();
    foreach ($tables as $table) {
      $id = sprintf('table:%s', $table->id());
      $derivatives[$id] = $base_plugin_def;
      $derivatives[$id]['label'] = $table->label();
      $derivatives[$id]['name'] = $table->getName();
      $derivatives[$id]['base_id'] = $table->getBaseId();
    }

    return $derivatives;
  }

}
