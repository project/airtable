<?php

declare(strict_types = 1);

namespace Drupal\airtable\Form\Table;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class EditForm.
 */
class EditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Update table');
    $actions['delete']['#value'] = $this->t('Delete table');

    return $actions;
  }

}
