<?php

declare(strict_types = 1);

namespace Drupal\airtable\Form\Table;

use Drupal\airtable\Entity\Table;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FormBase.
 */
abstract class FormBase extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\airtable\Entity\TableInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('The human-readable name of table. This can be different as the name on Airtable.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [Table::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $this->entity->getName(),
      '#description' => $this->t('The name of the table on Airtable.'),
      '#required' => TRUE,
    ];

    $form['base_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base ID'),
      '#default_value' => $this->entity->getBaseId(),
      '#description' => $this->t("An Airtable base contains all of the information you need for a particular project or collection. Each of the square icons on your homepage is a different base. It\'s kind of like a workbook in a traditional spreadsheet, and can contain multiple tables of content. It usually starts with app..."),
      '#required' => TRUE,
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $table = $this->entity;

    // Prevent leading and trailing spaces in airtable table labels.
    $table->set('label', trim($table->label()));

    switch ($table->save()) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created new table %label. Flush the cache if you want to use it as a Connector plugin.', ['%label' => $table->label()]));
        $this->logger('airtable')->info('Created new table %label.', ['%label' => $table->label()]);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('Updated table %label.', ['%label' => $table->label()]));
        $this->logger('airtable')->info('Updated table %label.', ['%label' => $table->label()]);
        break;
    }

    $form_state->setRedirectUrl($table->toUrl('collection'));
  }

}
