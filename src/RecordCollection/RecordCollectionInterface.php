<?php

declare(strict_types = 1);

namespace Drupal\airtable\RecordCollection;

use Countable;
use Iterator;

/**
 * Class RecordCollectionInterface.
 */
interface RecordCollectionInterface extends Iterator, Countable {}
