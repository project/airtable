<?php

declare(strict_types = 1);

namespace Drupal\airtable\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'airtable_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'airtable.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['api_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Airtable API key'),
      '#default_value' => $this->config('airtable.config')->get('key_id'),
      '#description' => $this->t(
        'To generate or manage your Airtable API key, visit your <a href=":link">account</a> page.',
        [':link' => Url::fromUri('https://airtable.com/account')->toString()]
      ),
      '#key_filters' => [
        'type' => 'authentication',
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    $this->config('airtable.config')
      ->set('key_id', $values['api_key'] ?? '')
      ->save();
  }

}
