<?php

declare(strict_types = 1);

namespace Drupal\airtable\Form\Table;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class AddForm.
 */
class AddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save and manage tables');

    return $actions;
  }

}
