<?php

declare(strict_types = 1);

namespace Drupal\airtable\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Table entity.
 *
 * @ConfigEntityType(
 *   id = "airtable_table",
 *   label = @Translation("Airtable table"),
 *   handlers = {
 *     "list_builder" = "Drupal\airtable\Controller\TableListBuilder",
 *     "form" = {
 *       "add" = "Drupal\airtable\Form\Table\AddForm",
 *       "edit" = "Drupal\airtable\Form\Table\EditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *   },
 *   admin_permission = "administer airtable",
 *   config_prefix = "airtable_table",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/airtable",
 *     "add-form" = "/admin/config/services/airtable/add",
 *     "edit-form" = "/admin/config/services/airtable/{airtable_table}/edit",
 *     "delete-form" = "/admin/config/services/airtable/{airtable_table}/delete"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "name",
 *     "base_id",
 *   }
 * )
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Table extends ConfigEntityBase implements TableInterface {

  /**
   * The Table ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Table label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Table name.
   *
   * @var string
   */
  protected $name;

  /**
   * The Table base ID.
   *
   * @var string
   */
  protected $base_id;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name): TableInterface {
    $this->set('name', $name);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseId() {
    return $this->get('base_id');
  }

  /**
   * {@inheritdoc}
   */
  public function setBaseId($baseId): TableInterface {
    $this->set('base_id', $baseId);

    return $this;
  }

}
