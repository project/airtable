<?php

declare(strict_types = 1);

namespace Drupal\airtable\RecordCollection;

use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;
use ArrayIterator;
use DavidZadrazil\AirtableApi\Response;
use Drupal\Component\Utility\NestedArray;
use Drupal\migrate\Row;

/**
 * Class RecordCollection.
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class RecordCollection implements RecordCollectionInterface {

  /**
   * The Airtable response.
   *
   * @var \DavidZadrazil\AirtableApi\Response
   */
  protected $response;

  /**
   * The Airtable summary response.
   *
   * @var \DavidZadrazil\AirtableApi\Response
   */
  protected $summary;

  /**
   * The item selector.
   *
   * @var array
   */
  protected $itemSelector = [];

  /**
   * The airtable records.
   *
   * @var \DavidZadrazil\AirtableApi\Record[]
   */
  protected $records;

  /**
   * RecordCollection constructor.
   *
   * @param \DavidZadrazil\AirtableApi\Response $response
   *   The Airtable response.
   * @param \DavidZadrazil\AirtableApi\Response $summary
   *   The summary response.
   * @param string $itemSelector
   *   The item selector.
   */
  public function __construct(Response $response, Response $summary, $itemSelector = '') {
    $this->response = $response;
    $this->summary = $summary;

    if ($itemSelector) {
      $this->itemSelector = explode(Row::PROPERTY_SEPARATOR, $itemSelector);
    }

    $this->records = new ArrayIterator($response->getRecords());
  }

  /**
   * {@inheritdoc}
   */
  public function current() {
    /** @var \DavidZadrazil\AirtableApi\Record $record */
    $record = $this->records->current();
    $record = $record->toArray();
    $record = json_decode(json_encode($record, JSON_FORCE_OBJECT), TRUE);

    if ($this->itemSelector) {
      $record = NestedArray::getValue($record, $this->itemSelector);
    }

    $record = $record ?? [];
    if (!empty($record['fields'])) {
      array_walk($record['fields'], static function (&$item) {
        if (is_array($item)) {
          $item = array_reverse($item);
        }
      });
    }

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function next():void {
    $this->records->next();
    if ($this->response->offset && empty($this->records->current())) {
      $this->response = $this->response->nextPage();
      foreach ($this->response->getRecords() as $record) {
        $this->records->append($record);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function key() {
    /** @var \DavidZadrazil\AirtableApi\Record $record */
    $record = $this->records->current();

    return $record->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function valid(): bool {
    return !empty($this->records->current());
  }

  /**
   * {@inheritdoc}
   */
  public function rewind(): void {
    $this->records->rewind();
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    /** @var \DavidZadrazil\AirtableApi\Record[] $summary */
    $summary = $this->summary->getRecords();
    $record = reset($summary);

    return $record ? current($record->getFields()) : 0;
  }

}
